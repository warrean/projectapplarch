/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.beans;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import krediet.entitybeans.Employee;
import krediet.entitybeans.Krediet;
import krediet.entitybeans.Onkost;
import krediet.entitybeans.Statusonkost;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Stateless
public class KredietStatelessBean implements KredietStatelessBeanRemote {
    @PersistenceContext
    private EntityManager em; //De Manager voor de Entity Beans: wordt gebruikt om gegevens uit de DB-tabellen te halen en in Java Klasses te vormen 

    @Override
    public Object getEmployee(int employeeid) {
        Employee manager = em.find(Employee.class, new Integer(employeeid));
        em.refresh(manager);
        return manager;
    }

    @Override
    public List getKredietenVoorEmployee(Object employeeObj, BigDecimal bedrag) {
        Employee employee = (Employee) employeeObj;
        List<Krediet> list = em.createNamedQuery("Krediet.findKredietVoorOnkost", Krediet.class)
                              .setParameter("employee", employee)
                              .setParameter("boss", employee.getEbaasid())
                              .setParameter("bedrag", bedrag)
                              .getResultList();
        Iterator it=list.iterator();
        while(it.hasNext()){
            em.refresh(it.next());
        }
        return list;
    }

    @Override
    public Object getKredietById(int kredietId) {
        Krediet krediet;
        krediet=em.find(Krediet.class, new Integer(kredietId));
        em.refresh(krediet); // echt nodig, alleen met refresh(), wordt nieuwe data van databank upgeload.
        return krediet;
    }

    @Override
    public List getKredieten() {
        
        List<Krediet> list = em.createNamedQuery("Krediet.findAll",Krediet.class).getResultList();
        Iterator it=list.iterator();
        while(it.hasNext()){
            em.refresh(it.next());
        }
	return list;
    }
    
    @Override
    public int getKeurenAantal(int kredietId,String s){
       
      Statusonkost statusonkost = em.createNamedQuery("Statusonkost.findBySnaam", Statusonkost.class)
                                      .setParameter("snaam", s).getSingleResult();
      
       Krediet krediet=(Krediet)this.getKredietById(kredietId);
       List<Onkost> list = (List<Onkost>) krediet.getOnkostCollection();
       Iterator it=list.iterator();
       int i=0;
       
       while(it.hasNext()){
             if(((Onkost)it.next()).getOstatusid().equals(statusonkost)){
                 i++;
             }
       }
       return i;
       
    }
    
    @Override
    public BigDecimal getKeurenTotaalBedrag(int kredietId,String s){
        BigDecimal big=new BigDecimal(0.00);
        Onkost onkost;
        Statusonkost statusonkost = em.createNamedQuery("Statusonkost.findBySnaam", Statusonkost.class)
                                      .setParameter("snaam", s).getSingleResult();
      
       Krediet krediet=(Krediet)this.getKredietById(kredietId);
       List<Onkost> list = (List<Onkost>) krediet.getOnkostCollection();
       Iterator it=list.iterator();

       
       while(it.hasNext()){
           onkost=(Onkost) it.next();
           
             if(onkost.getOstatusid().equals(statusonkost)){
                 big = big.add(onkost.getObedrag());
             }
       }
        
        return big;
    }
    
    

    
}
