/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.beans;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import krediet.entitybeans.Employee;
import krediet.entitybeans.Krediet;
import krediet.entitybeans.Onkost;
import krediet.entitybeans.Statusonkost;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Stateless
public class OnkostStatelessBean implements OnkostStatelessBeanRemote {
    @PersistenceContext
    private EntityManager em; //De Manager voor de Entity Beans: wordt gebruikt om gegevens uit de DB-tabellen te halen en in Java Klasses te vormen 

    @Override
    public Onkost maakNieuweOnkostAan(Object oeigenaarObj) {
        Integer maxonid = em.createNamedQuery("Onkost.findMinBeschikbaarLeegOnid",Integer.class).getSingleResult();
        int nieuwonid;
        
        if(maxonid==null){
            
            nieuwonid=1;// voor geval: 2346, dan oid=1 niet 5
        }else{
            
            nieuwonid=maxonid.intValue(); // oid is het eerste beschikbaar nr bv: 1245, dan oid=3
        }
        
        Employee oeigenaar = (Employee) oeigenaarObj;
        Statusonkost ostatus=em.createNamedQuery("Statusonkost.findBySnaam", Statusonkost.class)
                                .setParameter("snaam", "in aanmaak").getSingleResult();

        Onkost nieuwonkost=new Onkost(new Integer(nieuwonid),oeigenaar,ostatus);
        em.persist(nieuwonkost);
        return nieuwonkost;
    }
    
    @Override
    public void onkostOpslaan(String onid, String odatum, String obedrag, String obeschrijving) {
        
        SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy");
        Date odate = null;
        try {
            odate = df.parse(odatum);
        } catch (ParseException ex) {
            Logger.getLogger(OnkostStatelessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Onkost onkost = em.find(Onkost.class, new Integer(onid));
        
        onkost.setOdatum(odate);
        onkost.setObedrag(new BigDecimal(obedrag));
        onkost.setObeschrijving(obeschrijving);

        em.flush();
    }
    
    @Override
    public void onkostVerwijderen(String onid){
        Onkost onkost = em.find(Onkost.class, new Integer(onid));
        em.remove(onkost);
        em.flush();
    }
    

    @Override
    public List getOnkostenByEigenaarId(Object employeeObj) {
        Employee employee = (Employee) employeeObj;
        List<Onkost> list = em.createNamedQuery("Onkost.findOnkostVoorEmployee", Onkost.class)
                                    .setParameter("employee", employee).getResultList(); 
        Iterator it=list.iterator();
        while(it.hasNext()){
            em.refresh(it.next());
        }
        return list;
    }

    @Override
    public Onkost getOnkostById(int onkostId) {
        Onkost onkost = em.find(Onkost.class, new Integer(onkostId));
        em.refresh(onkost);
        return onkost;
    }
    
    @Override
    public List getOnkostenVoorGoedkeuring(Object managerObj) {
        Employee manager = (Employee) managerObj;
        List<Onkost> list = em.createNamedQuery("Onkost.findOnkostGoedtekeuren", Onkost.class)
                              .setParameter("employee", manager).getResultList();
        Iterator it=list.iterator();
        while(it.hasNext()){
            em.refresh(it.next());
        }
        return list;
    }
    
    @Override
    public void keurOnkostenAf(List onkosten) {
        // get status
        Statusonkost statusonkost = em.createNamedQuery("Statusonkost.findBySnaam", Statusonkost.class)
                                      .setParameter("snaam", "afgekeurd").getSingleResult();
        // update onkosten
        Onkost onkost;
        Krediet krediet;
        for (Object obj : onkosten) {
            onkost  = em.find(Onkost.class, ((Onkost)obj).getOnid());// hierdoor wordt dit object zeker in de persistentie context geplaatst
            em.refresh(onkost);
            krediet = onkost.getOkredietid();
            em.refresh(krediet);
            System.out.printf("KEUR AF: oud %f + bedrag %f = %f", krediet.getKsaldo(), onkost.getObedrag(), krediet.getKsaldo().add( onkost.getObedrag() ));
            krediet.setKsaldo( krediet.getKsaldo().add( onkost.getObedrag() ));
            onkost.setOstatusid(statusonkost);
            em.flush();  //synchroniseer de pers. context met de DB
        }
    }
    
    @Override
    public void keurOnkostenGoed(List onkosten) {
        // get status
        Statusonkost statusonkost = em.createNamedQuery("Statusonkost.findBySnaam", Statusonkost.class)
                                      .setParameter("snaam", "goedgekeurd").getSingleResult();
        
        // update onkosten & krediet
        Onkost onkost;
        Krediet krediet;
        for (Object obj : onkosten) {
            
            onkost  = em.find(Onkost.class, ((Onkost)obj).getOnid()); // hierdoor wordt dit object zeker in de persistentie context geplaatst
            em.refresh(onkost);
            onkost.setOstatusid(statusonkost);
            em.flush();  //synchroniseer de pers. context met de DB
           
        }
    }

    @Override
    public void stuurOnkostDoor(Object onkostObj, Object kredietObj) {
        // get status
        Statusonkost statusonkost = em.createNamedQuery("Statusonkost.findBySnaam", Statusonkost.class)
                                      .setParameter("snaam", "doorgestuurd").getSingleResult();
        // update onkost
        Onkost onkost   = em.find(Onkost.class,  ((Onkost)onkostObj).getOnid()); // hierdoor wordt dit object zeker in de persistentie context geplaatst
        Krediet krediet = em.find(Krediet.class, ((Krediet)kredietObj).getKid());
        em.refresh(onkost);
        em.refresh(krediet);
        onkost.setOkredietid(krediet);
        onkost.setOstatusid(statusonkost);
        krediet.setKsaldo( krediet.getKsaldo().subtract( onkost.getObedrag() ) );
        em.flush();
    }

}
