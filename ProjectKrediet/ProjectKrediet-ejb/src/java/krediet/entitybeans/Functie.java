/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.entitybeans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Entity
@Table(name = "functie")
@NamedQueries({
    @NamedQuery(name = "Functie.findAll", query = "SELECT f FROM Functie f"),
    @NamedQuery(name = "Functie.findByFid", query = "SELECT f FROM Functie f WHERE f.fid = :fid"),
    @NamedQuery(name = "Functie.findByFnaam", query = "SELECT f FROM Functie f WHERE f.fnaam = :fnaam")})
public class Functie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fid")
    private Integer fid;
    @Size(max = 20)
    @Column(name = "fnaam")
    private String fnaam;
    @ManyToMany(mappedBy = "functieCollection")
    private Collection<Employee> employeeCollection;

    public Functie() {
    }

    public Functie(Integer fid) {
        this.fid = fid;
    }

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public String getFnaam() {
        return fnaam;
    }

    public void setFnaam(String fnaam) {
        this.fnaam = fnaam;
    }

    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fid != null ? fid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Functie)) {
            return false;
        }
        Functie other = (Functie) object;
        if ((this.fid == null && other.fid != null) || (this.fid != null && !this.fid.equals(other.fid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "krediet.entitybeans.Functie[ fid=" + fid + " ]";
    }
    
}
