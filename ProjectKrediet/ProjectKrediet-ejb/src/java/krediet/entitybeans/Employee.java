/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.entitybeans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Entity
@Table(name = "employee")
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
    @NamedQuery(name = "Employee.findByEid", query = "SELECT e FROM Employee e WHERE e.eid = :eid"),
    @NamedQuery(name = "Employee.findByEnaam", query = "SELECT e FROM Employee e WHERE e.enaam = :enaam"),
    @NamedQuery(name = "Employee.findByEpaswoord", query = "SELECT e FROM Employee e WHERE e.epaswoord = :epaswoord")})
public class Employee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "eid")
    private Integer eid;
    @Size(max = 20)
    @Column(name = "enaam")
    private String enaam;
    @Size(max = 20)
    @Column(name = "epaswoord")
    private String epaswoord;
    
    
    @JoinTable(name = "employee_functie", joinColumns = {
        @JoinColumn(name = "efeid", referencedColumnName = "eid")}, inverseJoinColumns = {
        @JoinColumn(name = "effid", referencedColumnName = "fid")})
    @ManyToMany
    private Collection<Functie> functieCollection;
    
    
    @OneToMany(mappedBy = "kbeheerderid")
    private Collection<Krediet> kredietCollection;
    
    
    @OneToMany(mappedBy = "ebaasid")
    private Collection<Employee> employeeCollection;
    @JoinColumn(name = "ebaasid", referencedColumnName = "eid")
    @ManyToOne
    private Employee ebaasid;
    
    
    @OneToMany(mappedBy = "oeigenaarid")
    private Collection<Onkost> onkostCollection;

    public Employee() {
    }

    public Employee(Integer eid) {
        this.eid = eid;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getEnaam() {
        return enaam;
    }

    public void setEnaam(String enaam) {
        this.enaam = enaam;
    }

    public String getEpaswoord() {
        return epaswoord;
    }

    public void setEpaswoord(String epaswoord) {
        this.epaswoord = epaswoord;
    }

    public Collection<Functie> getFunctieCollection() {
        return functieCollection;
    }

    public void setFunctieCollection(Collection<Functie> functieCollection) {
        this.functieCollection = functieCollection;
    }

    public Collection<Krediet> getKredietCollection() {
        return kredietCollection;
    }

    public void setKredietCollection(Collection<Krediet> kredietCollection) {
        this.kredietCollection = kredietCollection;
    }

    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    public Employee getEbaasid() {
        return ebaasid;
    }

    public void setEbaasid(Employee ebaasid) {
        this.ebaasid = ebaasid;
    }

    public Collection<Onkost> getOnkostCollection() {
        return onkostCollection;
    }

    public void setOnkostCollection(Collection<Onkost> onkostCollection) {
        this.onkostCollection = onkostCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eid != null ? eid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.eid == null && other.eid != null) || (this.eid != null && !this.eid.equals(other.eid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return enaam;
    }
    
}
