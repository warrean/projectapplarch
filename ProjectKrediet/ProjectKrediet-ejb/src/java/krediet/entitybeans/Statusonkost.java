/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.entitybeans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Entity
@Table(name = "statusonkost")
@NamedQueries({
    @NamedQuery(name = "Statusonkost.findAll", query = "SELECT s FROM Statusonkost s"),
    @NamedQuery(name = "Statusonkost.findBySid", query = "SELECT s FROM Statusonkost s WHERE s.sid = :sid"),
    @NamedQuery(name = "Statusonkost.findBySnaam", query = "SELECT s FROM Statusonkost s WHERE s.snaam = :snaam")})
public class Statusonkost implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "sid")
    private Integer sid;
    @Size(max = 50)
    @Column(name = "snaam")
    private String snaam;
    @OneToMany(mappedBy = "ostatusid")
    private Collection<Onkost> onkostCollection;

    public Statusonkost() {
    }

    public Statusonkost(Integer sid) {
        this.sid = sid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSnaam() {
        return snaam;
    }

    public void setSnaam(String snaam) {
        this.snaam = snaam;
    }

    public Collection<Onkost> getOnkostCollection() {
        return onkostCollection;
    }

    public void setOnkostCollection(Collection<Onkost> onkostCollection) {
        this.onkostCollection = onkostCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sid != null ? sid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statusonkost)) {
            return false;
        }
        Statusonkost other = (Statusonkost) object;
        if ((this.sid == null && other.sid != null) || (this.sid != null && !this.sid.equals(other.sid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return snaam;
    }    
}
