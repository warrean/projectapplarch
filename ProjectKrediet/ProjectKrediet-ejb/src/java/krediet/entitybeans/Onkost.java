/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.entitybeans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Entity
@Table(name = "onkost")
@NamedQueries({
    // records die een van een bepaalde kredietbeheerder zijn en status 'doorgestuurd' (id = 2) hebben
    @NamedQuery(name  = "Onkost.findOnkostGoedtekeuren",
                query = "SELECT o FROM Onkost o " +
                        "WHERE  o.okredietid = (SELECT k.kid FROM Krediet k "
                        + "                     WHERE k.kbeheerderid = :employee)"
                        + " AND o.ostatusid.sid = 2"), 
    @NamedQuery(name  = "Onkost.findOnkostVoorEmployee",
                query = "SELECT o FROM Onkost o WHERE o.oeigenaarid = :employee"),

    @NamedQuery(name = "Onkost.findLastOnid", query = "SELECT MAX(o.onid) FROM Onkost o"),
    
        /*
         * Deze zorgt ervoor dat we de kleinst beschikbare ID gebruiken. 
         * Dat wilt zeggen als ID 9, 10, 11 beschikbaar zijn, en we verwijderen 10, dan zal 
         *  de volgende beschikbare ID terug 10 zijn. (ipv 12)
         */
    @NamedQuery(name = "Onkost.findMinBeschikbaarLeegOnid", query = "SELECT Min(a.onid)+1 FROM Onkost a WHERE (a.onid+1) NOT IN (SELECT b.onid FROM Onkost b) AND EXISTS (SELECT c.onid FROM Onkost c WHERE c.onid =1)")
})
public class Onkost implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "onid")
    private Integer onid;
    @Column(name = "odatum")
    @Temporal(TemporalType.DATE)
    private Date odatum;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "obedrag")
    private BigDecimal obedrag;
    @Size(max = 50)
    @Column(name = "obeschrijving")
    private String obeschrijving;
    
    @JoinColumn(name = "oeigenaarid", referencedColumnName = "eid")
    @ManyToOne
    private Employee oeigenaarid;
    
    @JoinColumn(name = "ostatusid", referencedColumnName = "sid")
    @ManyToOne
    private Statusonkost ostatusid;
    
    @JoinColumn(name = "okredietid", referencedColumnName = "kid")
    @ManyToOne
    private Krediet okredietid;

    public Onkost() {
    }

    public Onkost(Integer onid) {
        this.onid = onid;
    }

    public Onkost(Integer onid,Employee oeigenaarid,Statusonkost ostatusid) {
        this.onid = onid;
        this.oeigenaarid = oeigenaarid;
        this.ostatusid=ostatusid;
    }

    public Integer getOnid() {
        return onid;
    }

    public void setOnid(Integer onid) {
        this.onid = onid;
    }

    public Date getOdatum() {
        return odatum;
    }

    public void setOdatum(Date odatum) {
        this.odatum = odatum;
    }

    public BigDecimal getObedrag() {
        return obedrag;
    }

    public void setObedrag(BigDecimal obedrag) {
        this.obedrag = obedrag;
    }

    public String getObeschrijving() {
        return obeschrijving;
    }

    public void setObeschrijving(String obeschrijving) {
        this.obeschrijving = obeschrijving;
    }

    public Employee getOeigenaarid() {
        return oeigenaarid;
    }

    public void setOeigenaarid(Employee oeigenaarid) {
        this.oeigenaarid = oeigenaarid;
    }

    public Statusonkost getOstatusid() {
        return ostatusid;
    }

    public void setOstatusid(Statusonkost ostatusid) {
        this.ostatusid = ostatusid;
    }

    public Krediet getOkredietid() {
        return okredietid;
    }

    public void setOkredietid(Krediet okredietid) {
        this.okredietid = okredietid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (onid != null ? onid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Onkost)) {
            return false;
        }
        Onkost other = (Onkost) object;
        if ((this.onid == null && other.onid != null) || (this.onid != null && !this.onid.equals(other.onid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Onkost: " + obeschrijving ;
    }
    
    public void onkostDetailAfdrukken(){
        System.out.print("*OnkostID: "+this.getOnid()+"\t");
        System.out.print("*Datum: "+this.getOdatum().toString().substring(4, 10)+" "+(this.getOdatum().getYear()+1900)+"\t");
        System.out.print("*Beschrijving: "+this.getObeschrijving()+"\n");
        System.out.print("*OnkostMaker: "+this.getOeigenaarid()+"\t");
        System.out.print("*Bedrag: "+this.getObedrag()+"\t");
        System.out.print("*Status: "+this.getOstatusid()+"\n");
        System.out.println("-------------------------------------------------------------------------------------------");
    }
    
}
