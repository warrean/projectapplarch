/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.entitybeans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Entity
@Table(name = "krediet")
@NamedQueries({
    @NamedQuery(name = "Krediet.findAll", query = "SELECT k FROM Krediet k "),
    @NamedQuery(name = "Krediet.findKredietVoorOnkost", query = "SELECT k FROM Krediet k "
                                                              + "WHERE     (k.kbeheerderid = :employee "
                                                              + "           OR k.kbeheerderid = :boss) "
                                                              + "      AND (k.kisgewaarborgd = true OR"
                                                              + "           k.ksaldo - :bedrag >=0)")
})
public class Krediet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "kid")
    private Integer kid;
    @Size(max = 50)
    @Column(name = "kbeschrijving")
    private String kbeschrijving;
    @Column(name = "kisgewaarborgd")
    private Boolean kisgewaarborgd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ksaldo")
    private BigDecimal ksaldo;
    
    @JoinColumn(name = "kbeheerderid", referencedColumnName = "eid")
    @ManyToOne
    private Employee kbeheerderid;
    
    @OneToMany(mappedBy = "okredietid", fetch = FetchType.EAGER)
    private Collection<Onkost> onkostCollection;

    public Krediet() {
    }

    public Krediet(Integer kid) {
        this.kid = kid;
    }

    public Integer getKid() {
        return kid;
    }

    public void setKid(Integer kid) {
        this.kid = kid;
    }

    public String getKbeschrijving() {
        return kbeschrijving;
    }

    public void setKbeschrijving(String kbeschrijving) {
        this.kbeschrijving = kbeschrijving;
    }

    public Boolean getKisgewaarborgd() {
        return kisgewaarborgd;
    }

    public void setKisgewaarborgd(Boolean kisgewaarborgd) {
        this.kisgewaarborgd = kisgewaarborgd;
    }

    public BigDecimal getKsaldo() {
        return ksaldo;
    }

    public void setKsaldo(BigDecimal ksaldo) {
        this.ksaldo = ksaldo;
    }

    public Employee getKbeheerderid() {
        return kbeheerderid;
    }

    public void setKbeheerderid(Employee kbeheerderid) {
        this.kbeheerderid = kbeheerderid;
    }

    public Collection<Onkost> getOnkostCollection() {
        return onkostCollection;
    }

    public void setOnkostCollection(Collection<Onkost> onkostCollection) {
        this.onkostCollection = onkostCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kid != null ? kid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Krediet)) {
            return false;
        }
        Krediet other = (Krediet) object;
        if ((this.kid == null && other.kid != null) || (this.kid != null && !this.kid.equals(other.kid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getKbeschrijving();
    }
    
    public void kredietDetailAfdrukken(){
      System.out.println("-------------------------------------------------------------------------------------------");
      System.out.println("-------------------------------------------------------------------------------------------");
      System.out.print("-KredietID: "+this.getKid()+"\t");
      System.out.print("-Beschrijving: "+this.getKbeschrijving()+"\t");
      System.out.print("-Beheerder: "+this.getKbeheerderid()+"\n");
      System.out.print("-Gewaarborgd: "+this.getKisgewaarborgd()+"\t");
      System.out.print("-Saldo: "+this.getKsaldo()+"\n");
      System.out.println("-------------------------------------------------------------------------------------------");
      System.out.println("-------------------------------------------------------------------------------------------");
    }
    
}
