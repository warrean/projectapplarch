/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.ctrl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import krediet.beans.KredietStatelessBeanRemote;
import krediet.beans.OnkostStatelessBeanRemote;
import krediet.entitybeans.Employee;
import krediet.entitybeans.Krediet;
import krediet.entitybeans.Onkost;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
public class BoekhouderCtrl extends HttpServlet {
    @EJB
    private OnkostStatelessBeanRemote onkostStatelessBean;
    @EJB
    private KredietStatelessBeanRemote kredietStatelessBean;

    private enum Actie {
        overzicht, kredietdetail,onkostdetail,terug
    }

    protected void gotoPage(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher(url);
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("BoekhouderController - doGet");
        HttpSession session = request.getSession();
        session.setAttribute("principalName", request.getUserPrincipal()); //TODO lookup name
        Onkost onkost;
        String actionParam = request.getParameter("actie");
        Actie actie;
        if (actionParam != null) {
            actie = Actie.valueOf(actionParam.toLowerCase());
        } else {
            actie = Actie.overzicht;
        }

        switch (actie) {
            case overzicht:
                session.setAttribute("kredietList", kredietStatelessBean.getKredieten());
                gotoPage("boekhouder/kredietoverzicht.jsp", request, response);
		break;
            case kredietdetail:
                int id=Integer.parseInt(request.getParameter("kredietId"));
		Krediet krediet = (Krediet) kredietStatelessBean.getKredietById(id);
                List<Onkost> onkostLijst = (List<Onkost>) krediet.getOnkostCollection();
                int goed=kredietStatelessBean.getKeurenAantal(id, "goedgekeurd");
                int af=kredietStatelessBean.getKeurenAantal(id, "afgekeurd");
                int door=kredietStatelessBean.getKeurenAantal(id, "doorgestuurd");
                
                BigDecimal doorg=kredietStatelessBean.getKeurenTotaalBedrag(id, "doorgestuurd");
                BigDecimal goedg=kredietStatelessBean.getKeurenTotaalBedrag(id, "goedgekeurd");
                session.setAttribute("goed", goed);
                session.setAttribute("af", af);
                session.setAttribute("door", door);
                
                session.setAttribute("doorg", doorg);
                session.setAttribute("goedg", goedg);
                
                session.setAttribute("manager", false);
                session.setAttribute("boekhouder", true);
                session.setAttribute("urldetail", "boekhouder");
                session.setAttribute("krediet", krediet);
                session.setAttribute("onkostList", onkostLijst);
                gotoPage("boekhouder/kredietdetail.jsp", request, response);
		break;
            case onkostdetail:
                onkost = (Onkost) onkostStatelessBean.getOnkostById(Integer.parseInt(request.getParameter("onkostId")));
                session.setAttribute("werknemer", false);
                session.setAttribute("manager", false);
                session.setAttribute("boekhouder", true);
                session.setAttribute("nieuw", false);
                session.setAttribute("inaanmaak", false);
                session.setAttribute("onkost", onkost);
                gotoPage("onkost/detail.jsp", request, response);
                break;
            case terug:
                response.sendRedirect("boekhouder");
                break;
	}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
