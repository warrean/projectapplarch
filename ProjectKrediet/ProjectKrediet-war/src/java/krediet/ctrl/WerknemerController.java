/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krediet.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import krediet.beans.KredietStatelessBeanRemote;

import krediet.beans.OnkostStatelessBeanRemote;
import krediet.entitybeans.Employee;
import krediet.entitybeans.Krediet;
import krediet.entitybeans.Onkost;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
public class WerknemerController extends HttpServlet {

    @EJB
    private OnkostStatelessBeanRemote onkostStatelessBean;
    @EJB
    private KredietStatelessBeanRemote kredietStatelessBean;

    private enum Actie {
        overzicht, onkostdetail,onkostaanmaken,
        doorsturen, doorsturenfin,opslaan,verwijderen,terug,annuleren
    }

    protected void gotoPage(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher(url);
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Onkost onkost;
        Krediet krediet;
        HttpSession session = request.getSession();
        session.setAttribute("principalName", request.getUserPrincipal()); //TODO lookup name

        String actionParam = request.getParameter("actie");
        Actie actie;
        if (actionParam != null) {
            actie = Actie.valueOf(actionParam.toLowerCase());
        } else {
            actie = Actie.overzicht;//voor default response.sendRedirect("werknemer");
        }

        switch (actie) {
            case overzicht:
                session.setAttribute("onkostList", onkostStatelessBean.getOnkostenByEigenaarId(getEmployee(request)));
                gotoPage("onkost/overzicht.jsp", request, response);
                break;
            case onkostdetail:
                onkost = (Onkost) onkostStatelessBean.getOnkostById(Integer.parseInt(request.getParameter("onkostId")));
                session.setAttribute("werknemer", true);
                session.setAttribute("manager", false);
                session.setAttribute("boekhouder", false);
                session.setAttribute("nieuw", false);
                if ( onkost.getOstatusid().getSnaam().equals("in aanmaak") ) {
                        session.setAttribute("inaanmaak", true);
                } else {
                        session.setAttribute("inaanmaak", false);
                }
                session.setAttribute("onkost", onkost);
                gotoPage("onkost/detail.jsp", request, response);
                break;

            case onkostaanmaken:
                onkost=(Onkost)onkostStatelessBean.maakNieuweOnkostAan(getEmployee(request));
                session.setAttribute("werknemer", true);
                session.setAttribute("manager", false);
                session.setAttribute("boekhouder", false);
                session.setAttribute("onkost", onkost);
                session.setAttribute("inaanmaak", true);
                session.setAttribute("nieuw", true);
                gotoPage("onkost/detail.jsp", request, response);
                break;

            case opslaan:
                onkostStatelessBean.onkostOpslaan(request.getParameter("onkostId"), request.getParameter("odatum"), request.getParameter("obedrag"), request.getParameter("obeschrijving"));
                session.setAttribute("message", "De onkost werd opgeslagen.");
                response.sendRedirect("werknemer");
                break;
           
            case verwijderen:
                onkostStatelessBean.onkostVerwijderen(request.getParameter("onkostId"));
                session.setAttribute("message", "De onkost werd verwijderd.");
                response.sendRedirect("werknemer");
                break;

            case doorsturen:
                // Eerst de onkost opslaan wanneer er bijvoorbeeld wijzigingen gebeurd zijn
                onkostStatelessBean.onkostOpslaan(request.getParameter("onkostId"), request.getParameter("odatum"), request.getParameter("obedrag"), request.getParameter("obeschrijving"));
                
                // Vervolgens doorverwijzen naar krediet-selectie pagina
                onkost = (Onkost) onkostStatelessBean.getOnkostById(Integer.parseInt(request.getParameter("onkostId")));
                session.setAttribute("onkost", onkost);
                session.setAttribute("kredietList", kredietStatelessBean.getKredietenVoorEmployee( getEmployee(request), onkost.getObedrag()) );
                gotoPage("onkost/kredietselectie.jsp", request, response);
                break;
                
            case doorsturenfin:
                onkost = (Onkost) onkostStatelessBean.getOnkostById(Integer.parseInt(request.getParameter("onkostId")));
                krediet = (Krediet) kredietStatelessBean.getKredietById(Integer.parseInt(request.getParameter("kredietId")));
                
                onkostStatelessBean.stuurOnkostDoor(onkost, krediet);
                
                session.setAttribute("message", "Onkost werd aan krediet toegevoegd en doorgestuurd.");
                response.sendRedirect("werknemer");
                break;
            case terug:
                response.sendRedirect("werknemer");
                break;
            case annuleren:
                onkostStatelessBean.onkostVerwijderen(request.getParameter("onkostId"));
                response.sendRedirect("werknemer");
                break;
                
        }
    }

    private Employee getEmployee(HttpServletRequest request) {
        return (Employee) kredietStatelessBean.getEmployee(Integer.parseInt(request.getUserPrincipal().getName()));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
