/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import krediet.beans.KredietStatelessBeanRemote;
import krediet.entitybeans.Employee;

/**
 *
 * @author andywarrens & pei
 */
public class IndexController extends HttpServlet {
    @EJB
    private KredietStatelessBeanRemote kredietStatelessBean;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        System.out.println("---- INDEX ----");
        
        if (request.getUserPrincipal() != null) {
            Integer eid       = Integer.parseInt( request.getUserPrincipal().getName() );
            Employee employee = (Employee) kredietStatelessBean.getEmployee(eid);

            HttpSession session = request.getSession();
            session.setAttribute("loggedInName", employee.getEnaam() );
            
            RequestDispatcher view = request.getRequestDispatcher("index_.jsp");
            view.forward(request, response);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
