/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.ctrl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import krediet.beans.KredietStatelessBeanRemote;
import krediet.beans.OnkostStatelessBeanRemote;
import krediet.entitybeans.Employee;
import krediet.entitybeans.Krediet;
import krediet.entitybeans.Onkost;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
public class ManagerController extends HttpServlet {
    @EJB
    private KredietStatelessBeanRemote kredietStatelessBean;

    @EJB
    private OnkostStatelessBeanRemote onkostStatelessBean;
    
    private enum Actie {
        onkostkeuren, afkeuren, goedkeuren,onkostdetail,kredietoverzicht,terug
    }
    
    protected void gotoPage(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        RequestDispatcher view = request.getRequestDispatcher(url);
        view.forward(request,response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Onkost> onkostSelectie;
        HttpSession session = request.getSession();
        Onkost onkost;
        //actie=afkeuren&ids=3:4
        String actionParam = request.getParameter("actie");
        Actie actie;
        if (actionParam != null) {
            actie = Actie.valueOf(actionParam.toLowerCase());  
        } else {
            actie = Actie.onkostkeuren;
        }       
        switch (actie) {
            case onkostkeuren:
                Employee manager = (Employee) kredietStatelessBean.getEmployee(Integer.parseInt(request.getUserPrincipal().getName()));
                session.setAttribute("onkostList", onkostStatelessBean.getOnkostenVoorGoedkeuring(manager)); 
                gotoPage("manager/onkostgoedkeuren.jsp", request, response);
                break;
            case onkostdetail:
                onkost = (Onkost) onkostStatelessBean.getOnkostById(Integer.parseInt(request.getParameter("onkostId")));
                session.setAttribute("werknemer", false);
                session.setAttribute("manager", true);
                session.setAttribute("boekhouder", false);
                session.setAttribute("nieuw", false);
                session.setAttribute("inaanmaak", false);
                session.setAttribute("onkost", onkost);
                gotoPage("onkost/detail.jsp", request, response);
                break;
            case kredietoverzicht:
                int id=Integer.parseInt(request.getParameter("kredietId"));
		Krediet krediet = (Krediet) kredietStatelessBean.getKredietById(id);
                List<Onkost> onkostLijst = (List<Onkost>) krediet.getOnkostCollection();
                int goed=kredietStatelessBean.getKeurenAantal(id, "goedgekeurd");
                int af=kredietStatelessBean.getKeurenAantal(id, "afgekeurd");
                int door=kredietStatelessBean.getKeurenAantal(id, "doorgestuurd");
                
                BigDecimal doorg=kredietStatelessBean.getKeurenTotaalBedrag(id, "doorgestuurd");
                BigDecimal goedg=kredietStatelessBean.getKeurenTotaalBedrag(id, "goedgekeurd");
                session.setAttribute("goed", goed);
                session.setAttribute("af", af);
                session.setAttribute("door", door);
                
                session.setAttribute("doorg", doorg);
                session.setAttribute("goedg", goedg);
                
                session.setAttribute("manager", true);
                session.setAttribute("boekhouder", false);
                session.setAttribute("urldetail", "manager");
                session.setAttribute("krediet", krediet);
                session.setAttribute("onkostList", onkostLijst);
                gotoPage("boekhouder/kredietdetail.jsp", request, response);               
                break;
            case afkeuren:
                onkostSelectie = buildOnkostList(request.getParameter("ids"));                
                onkostStatelessBean.keurOnkostenAf(onkostSelectie);
                session.setAttribute("message", "De geselecteerde onkosten werden afgekeurd.");
                response.sendRedirect("manager");
                break;
            case goedkeuren:
                onkostSelectie = buildOnkostList(request.getParameter("ids"));                
                onkostStatelessBean.keurOnkostenGoed(onkostSelectie);
                session.setAttribute("message", "De geselecteerde onkosten werden goedgekeurd.");
                response.sendRedirect("manager");
                break; 
           case terug:
                response.sendRedirect("manager");
                break;
        }
    }
    
    private List<Onkost> buildOnkostList(String idParam) {
        String[] ids = idParam.split(":");
        List<Onkost> onkostListSelectie = new ArrayList<>();
        
        for (String id : ids) {
            int onkostid = Integer.parseInt(id);
            onkostListSelectie.add((Onkost) onkostStatelessBean.getOnkostById(onkostid));
        }
        
        return onkostListSelectie;
    } // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
