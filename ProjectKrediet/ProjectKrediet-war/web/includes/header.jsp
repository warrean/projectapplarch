<%-- 
    Document   : header
    Created on : Nov 24, 2014, 11:29:02 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${param.title}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="tcal/tcal.css" />
    </head>
    
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            
            <div class="navbar-header">
                <a class="navbar-brand" href="index.jsp">
                    <img alt="Brand" src="includes/images/logo_small.png" style="height: 100%">
                </a>
            </div>
            
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="logout">Uitloggen</a></li>
                </ul>
                
                <p class="navbar-text navbar-right">Ingelogd als <strong>${sessionScope.loggedInName}</strong> (personeel-id: ${pageContext.request.userPrincipal})</p>
            </div>
            
        </div>
    </nav>

