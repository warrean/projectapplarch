<%-- 
    Document   : message
    Created on : Dec 2, 2014, 1:49:29 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${sessionScope.message != null}">
    <div class="alert alert-info" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        ${sessionScope.message}
    </div>
</c:if>

<c:remove var="message" scope="session" /> <%-- na 1x tonen verwijder de message --%>
