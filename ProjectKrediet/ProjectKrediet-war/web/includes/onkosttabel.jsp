<%-- 
    Document   : onkosttabel
    Created on : Nov 24, 2014, 11:15:33 PM
    Author     :groep:Andy Warrens & Pei Tian
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table class="table table-striped table-hover" id="onkostOverzichtTabel" >
    <thead>
        <tr>
            <c:if test="${param.isSelectable != null}">
		<td></td>
            </c:if>
            <td><strong>Datum</strong></td>
            <td><strong>Beschrijving</strong></td>
            <td><strong>Bedrag</strong></td>
            <td><strong>Status</strong></td>
            <c:if test="${param.displayEmployeeColumn != null}">
                <td><strong>Onkost Maker</strong></td>
                <td><strong>Gelinkt Krediet</strong></td>
                <td><strong>Kredietbeheerder</strong></td>
            </c:if>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="onkost" items="${sessionScope.onkostList}">
            <tr data-onkostid="${onkost.onid}">
                <c:if test="${param.isSelectable != null}">
                    <td><input type="checkbox" name="rowSelectCheckbox"></td>
                </c:if> 
    
                <td class="toon" data-onkostid="${onkost.onid}"><fmt:formatDate pattern="dd-MM-yyyy" value="${onkost.getOdatum()}" /></td>
                <td class="toon" data-onkostid="${onkost.onid}">${onkost.getObeschrijving()}</td>
                <td class="toon" data-onkostid="${onkost.onid}">${onkost.getObedrag()}</td>
                <td class="toon" data-onkostid="${onkost.onid}">${onkost.getOstatusid()}</td>
                <c:if test="${param.displayEmployeeColumn != null}">
                    <td class="toon" data-onkostid="${onkost.onid}">${onkost.getOeigenaarid()}</td>
                    <td class="toon" data-onkostid="${onkost.onid}">${onkost.getOkredietid()}</td>
                    <td class="toon" data-onkostid="${onkost.onid}">${onkost.getOkredietid().getKbeheerderid()}</td>
                </c:if>     
            </tr>
        </c:forEach>
    </tbody>
</table>