<%-- 
    Document   : onkostgoedkeuren
    Created on : Nov 24, 2014, 11:14:07 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../includes/header.jsp">
    <jsp:param name="title" value="Onkosten Keuren"></jsp:param>
</jsp:include>

    <body>
        <div class="container">
            <h1>Manager <small>Onkosten Keuren</small></h2>
            
            <jsp:include page="../includes/message.jsp"></jsp:include>
            
            <c:choose>
                <c:when test="${sessionScope.onkostList.size() != 0}">
                    <p>
                        <jsp:include page="../includes/onkosttabel.jsp">
                        <jsp:param name="isSelectable" value="true"></jsp:param>
                        <jsp:param name="displayEmployeeColumn" value="true"></jsp:param>
                        </jsp:include>
                    </p>
                    <button type="button" class="btn btn-danger" id="btnAfkeuren">Afkeuren</button>
                    <button type="button" class="btn btn-success" id="btnGoedkeuren">Goedkeuren</button>
                    <a  href="index.jsp" role="button" class="btn btn-warning ">Terug (Home)</a> 
                </c:when> 
                <c:otherwise >
                    <div id="blink" align="center">
                        <strong>U heeft geen onkosten om goed te keuren!</strong>
                    </div> 
                    <script language="javascript"> 
                        function changeColor(){ 
                            var color="#f00|#0f0|#00f|#880|#808|#088|yellow|green|blue|gray"; 
                            color=color.split("|"); 
                            document.getElementById("blink").style.color=color[parseInt(Math.random() * color.length)]; 
                        } 
                        setInterval("changeColor()",200); 
                    </script>
                    <a  href="index.jsp" role="button" class="btn btn-warning ">Terug (Home)</a>
                </c:otherwise>
                </c:choose>
          </div>
            <script>
                
            $("#btnAfkeuren").click(function() {
                keurOnkosten(false);
            });
            $("#btnGoedkeuren").click(function() {
                keurOnkosten(true);
            });
            var keurOnkosten = function(isGoedkeuren) {
                //verzamel checkboxen
                var checkedOnkostIds = [];
                $("#onkostOverzichtTabel tr input:checked").each(function (i, el) {
                    checkedOnkostIds.push($(el.parentElement.parentElement).data("onkostid"));
                });
                if (checkedOnkostIds.length === 0) {
                    return;
                }
                //stuur door in lijst
                var actie  = isGoedkeuren ? "goedkeuren" : "afkeuren",
                    url    = "manager",
                    params = "?actie=" + actie + "&ids=" + checkedOnkostIds.join(":");
                document.location = url + params;               
            }
            </script>
            <script>
            $(".toon").click(function() {
                var onkostId = $(this).data("onkostid");
                document.location = "manager?actie=OnkostDetail&onkostId=" + onkostId;
            });
            </script>
    </body>
</html>
