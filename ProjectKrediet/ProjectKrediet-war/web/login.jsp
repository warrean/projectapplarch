<%-- 
    Document   : login
    Created on : Nov 16, 2014, 9:06:35 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inloggen</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    </head>
    </head>
    <body>
        <div class="container">
            
            <img src="includes/images/logo.jpg" width="100%">
            
            <h2>Welkom, gelieve uw gegevens in te vullen om in te loggen.</h2>
            
            <form method="post" action="j_security_check">
                
                <div class="form-group">
                  <label>Personeel id</label>
                  <input type="text" class="form-control" placeholder="Personeel id" name="j_username">
                </div>
                
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control"placeholder="Password" name="j_password" >
                </div>
                
                <input type="submit" class="btn btn-default" value="Login">
                
            </form>
            
        </div>
        
    </body>
</html>
