<%-- 
    Document   : overzicht
    Created on : Nov 16, 2014, 9:12:32 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>
<jsp:include page="../includes/header.jsp">
    <jsp:param name="title" value="Overzicht Onkosten"></jsp:param>
</jsp:include>

    <body>
        <div class="container">            
            <h1>Algemene werknemer <small>Onkosten overzicht</small></h1>
            
            <jsp:include page="../includes/message.jsp"></jsp:include>

            <jsp:include page="../includes/onkosttabel.jsp"></jsp:include>
            
            <script>
            $(".toon").click(function() {
                var onkostId = $(this).data("onkostid");
                document.location = "werknemer?actie=OnkostDetail&onkostId=" + onkostId;
            });
            </script>
            

            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <div class="col-sm-10">
                        <a href="werknemer?actie=onkostaanmaken" role="button" class="btn btn-success ">Nieuwe onkost toevoegen</a>
                        <a href="index.jsp" role="button" class="btn btn-warning ">Terug naar Home</a>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
