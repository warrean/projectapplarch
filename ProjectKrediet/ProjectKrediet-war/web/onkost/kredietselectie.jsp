<%-- 
    Document   : kredietselectie
    Created on : Nov 30, 2014, 11:02:23 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="../includes/header.jsp">
    <jsp:param name="title" value="Krediet Selecteren"></jsp:param>
</jsp:include>

    <body>
        <div class="container">
            <h2>Krediet selecteren voor ${sessionScope.onkost}</h2>
            <table  class="table table-hover">
                <thead>
                    <tr>
                        <td><strong>Beschrijving</strong></td>
                        <td><strong>Gewaarborgd</strong></td>
                        <td><strong>Saldo</strong></td>
                        <td><strong>Saldo na aftrekking van deze onkost</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="krediet" items="${sessionScope.kredietList}">
                        <tr data-kredietid="${krediet.kid}"  data-actie="DoorsturenFin" class="klik <c:if test='${krediet.ksaldo - sessionScope.onkost.obedrag<0}'>bg bg-danger</c:if>">
                            <td>${krediet.kbeschrijving}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${krediet.kisgewaarborgd == true}">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>${krediet.ksaldo}</td>
                            <td>${krediet.ksaldo - sessionScope.onkost.obedrag}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
           <input  data-kredietid="" data-actie="Terug" type="button" value="Annuleren" class="btn btn-warning klik" />
        </div>

        <script>
            $(".klik").click(function() {
                var kredietId = $(this).data("kredietid"),
                    actie  = $(this).data("actie"),
                    url    = "werknemer",
                    params = "?actie=" + actie + "&kredietId=" + kredietId + "&onkostId=" + ${sessionScope.onkost.onid};
                document.location = url + params;               
            });
        </script>
    </body>
</html>
