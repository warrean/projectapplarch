<%-- 
    Document   : detail
    Created on : Nov 20, 2014, 4:20:22 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../includes/header.jsp">
    <jsp:param name="title" value="Onkost Detail"></jsp:param>
</jsp:include>

<jsp:include page="../includes/tcal.jsp"></jsp:include>

    <div class="container">
        <h1>Algemene werknemer <small>Onkost Detail</small></h1>
        
        <c:if test="${sessionScope.werknemer == true}"> 
        <form class="form-horizontal" role="form" action="<c:url value='werknemer' />" method="get" name="displayform" onSubmit="return invullencheck()">
        </c:if>
        
        <c:if test="${sessionScope.manager == true}"> 
        <form class="form-horizontal" role="form" action="<c:url value='manager' />" method="get" >
        </c:if>
      
        <c:if test="${sessionScope.boekhouder == true}"> 
        <form class="form-horizontal" role="form" action="<c:url value='boekhouder' />" method="get">
        </c:if>
       
          <div class="form-group">
            <label for="datum" class="col-sm-2 control-label">Datum</label>
            <div class="col-sm-10">
              <input type="text" class="form-control tcal" name="odatum" id="datum" placeholder="dd-mm-yyyy" value="<fmt:formatDate pattern='dd-MM-yyyy' value='${onkost.odatum}'/>" <c:if test='${sessionScope.inaanmaak == false}'>disabled="disabled"</c:if> />
            </div>
          </div>

          <div class="form-group">
            <label for="bedrag" class="col-sm-2 control-label">Bedrag</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon">&#8364;</span>
                    <input type="text" name="obedrag" id="bedrag" class="form-control"  placeholder="0.00" value="${onkost.obedrag}" <c:if test='${sessionScope.inaanmaak == false}'>disabled="disabled"</c:if> />
                </div>
            </div>
          </div>

          <div class="form-group">
            <label for="beschrijving" class="col-sm-2 control-label">Beschrijving</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="obeschrijving" id="beschrijving" placeholder="Beschrijving" value="${onkost.obeschrijving}" <c:if test='${sessionScope.inaanmaak == false}'>disabled="disabled"</c:if> />
            </div>
          </div>

          <div class="form-group">
            <label for="eigenaar" class="col-sm-2 control-label">Onkost Maker</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" disabled="disabled" id="eigenaar" placeholder="Onkost Maker" value="${onkost.oeigenaarid}" />
            </div>
          </div>

          <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" disabled="disabled" id="status" placeholder="Status" value="${onkost.ostatusid}" />
            </div>
          </div>
            
          <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Gelinkt Krediet</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" disabled="disabled" id="status" placeholder="Nog GEEN krediet gelinkt" value="${onkost.okredietid}" />
            </div>  
            <div class="col-sm-2">
                <c:if test="${sessionScope.manager == true}"> 
                   <input data-kredietid="${onkost.getOkredietid().getKid()}" data-actie="kredietoverzicht" data-url="manager" type="button" value="Toon krediet detail" class="btn btn-info klik" />
                </c:if>
            </div>
          </div>
            
            
            <input type="hidden" name="onkostId" value="${onkost.onid}"/>
       
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <c:if test="${sessionScope.werknemer == true}"> 
                    <c:if test="${sessionScope.inaanmaak == true}">
                        <input type="submit" name="actie" value="Opslaan" class="btn btn-primary" />
                        <c:if test="${sessionScope.nieuw == true}">
                        <input data-kredietid="" data-actie="Annuleren" data-url="werknemer" type="button" value="Annuleren" class="btn btn-danger klik" />
                        </c:if>
                        <c:if test="${sessionScope.nieuw == false}">
                            <input data-kredietid="" data-actie="Verwijderen" data-url="werknemer" type="button" value="Verwijderen" class="btn btn-fail klik" />
                            <input type="submit" name="actie" value="Doorsturen" class="btn btn-success" />
                        </c:if>
                    </c:if>
                </c:if>
            </div>
          </div>
              
              
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <c:if test="${sessionScope.nieuw == false}">
                    <input data-kredietid="" data-actie="Terug" data-url="werknemer" type="button" value="Terug (Onkost overzicht)" class="btn btn-warning klik" />
                </c:if>
                        
                <c:if test="${sessionScope.manager == true}"> 
                   <input data-kredietid="${onkost.getOkredietid().getKid()}" data-actie="Terug" data-url="manager" type="button" value="Terug (Onkosten keuren)" class="btn btn-warning klik" />
                </c:if>

                <c:if test="${sessionScope.boekhouder == true}"> 
                    <input data-kredietid="${onkost.getOkredietid().getKid()}" data-actie="kredietdetail" data-url="boekhouder" type="button" value="Terug (Krediet detail)" class="btn btn-warning klik" />
                </c:if>
            </div>
          </div>
            
        </form>
   
            <script>
            $(".klik").click(function() {
                var actie=$(this).data("actie");
                var url=$(this).data("url");
                var kredietid=$(this).data("kredietid");
                document.location = url+"?actie="+actie+"&onkostId=" + ${onkost.onid}+"&kredietId="+kredietid;
            });
            </script>
    </div>
    </body>
</html>
