<%-- 
    Document   : kredietdetail
    
    Author     :groep:Andy Warrens & Pei Tian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../includes/header.jsp">
    <jsp:param name="title" value="Krediet Detail"></jsp:param>
</jsp:include>

    <body>
        <div class="container">
            <h1>
                <c:if test="${sessionScope.manager == true}">Manager</c:if>
                <c:if test="${sessionScope.boekhouder == true}">Boekhouder</c:if>
                <small>Krediet Detail</small>
            </h1>
                
                <div class="row">
                    <h3  class="col-sm-5">Krediet gegevens</h3>
                    <div class="col-sm-5">
                        <style type = "text/css" scoped>
                            .btn {
                                margin-left: 228px;
                                margin-top: 17px;
                            }
                        </style>
                        <c:if test="${sessionScope.manager == true}">    
                           <input data-actie="Terug" data-url="manager" type="button" value="Terug" class="btn btn-warning klik" />
                        </c:if>

                        <c:if test="${sessionScope.boekhouder == true}"> 
                            <input data-actie="Terug" data-url="boekhouder" type="button" value="Terug" class="btn btn-warning klik" />
                        </c:if>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-4">
                        <p>
                            <dl class="dl-horizontal">
                                <dt>Beschrijving</dt>
                                <dd>${krediet.kbeschrijving}</dd>

                                <dt>Is gewaarborgd</dt>
                                <dd>
                                    <c:choose>
                                        <c:when test="${krediet.kisgewaarborgd == true}">
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        </c:otherwise>
                                    </c:choose>
                                </dd>

                                <dt>Beheerder</dt>
                                <dd>${krediet.kbeheerderid}</dd>
                            </dl>
                        </p>
                        
                        <p>
                            <dl class="dl-horizontal">
                                <dt>Huidig saldo</dt>
                                <dd>${krediet.ksaldo}</dd>

                                <dt>Al reeds betaald</dt>
                                <dd>${goedg}</dd>

                                <dt>Resterend</dt>
                                <dd>${doorg}</dd>

                                <dt>Oorspronkelijk budget</dt>
                                <dd>${doorg+goedg+krediet.ksaldo}</dd>
                            </dl>
                        </p>
                    </div>
                    <div class="col-sm-6 col-sm-offset-1">
                        <p>
                            <dl class="dl-horizontal">
                                <dt style="width: 210px">Aantal onkosten goedgekeurd</dt>
                                <dd style="margin-left: 230px">${goed}</dd>

                                <dt style="width: 210px">Aantal onkosten afgekeurd</dt>
                                <dd style="margin-left: 230px">${af}</dd>

                                <dt style="width: 210px">Nog te keuren onkosten</dt>
                                <dd style="margin-left: 230px">${door}</dd>
                            </dl>
                        </p>
                    </div>
                </div>
          
        <h3>Krediet onkosten</h3>
	<jsp:include page="../includes/onkosttabel.jsp"></jsp:include>

        <script>
           $(".klik").click(function() {
                var actie=$(this).data("actie");
                var url=$(this).data("url");
                document.location = url+"?actie="+actie;
            });
          </script>
          <script>
            $(".toon").click(function() {
                var onkostId = $(this).data("onkostid");
                var ur="${sessionScope.urldetail}";
                document.location = ur+"?actie=OnkostDetail&onkostId=" +onkostId;
            });
        </script>
     </div>
    </body>
</html>
