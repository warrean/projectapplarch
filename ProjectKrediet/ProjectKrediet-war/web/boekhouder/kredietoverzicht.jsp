
<%-- 
    Document   : kredietoverzicht
   
    Author     :groep:Andy Warrens & Pei Tian
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../includes/header.jsp">
    <jsp:param name="title" value="Krediet Overzicht"></jsp:param>
</jsp:include>

    <body>
        <div class="container">
            <h1>Boekhouder <small>Krediet Overzicht</small></h1>
            <table id="kredietTabel" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td><strong>Beschrijving</strong></td>
                        <td><strong>Gewaarborgd</strong></td>
                        <td><strong>Saldo</strong></td>
                        <td><strong>KredietBeheerder</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="krediet" items="${sessionScope.kredietList}">
                        <tr data-kredietid="${krediet.kid}">
                            <td>${krediet.kbeschrijving}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${krediet.kisgewaarborgd == true}">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>${krediet.ksaldo}</td>
                            <td>${krediet.kbeheerderid}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <a href="index.jsp" role="button" class="btn btn-warning ">Terug naar Home</a>
        </div>

        <script>
            $("#kredietTabel tr").click(function() {
                var kredietId = $(this).data("kredietid"),
                    actie  = "kredietDetail"
                    url    = "boekhouder",
                    params = "?actie=" + actie + "&kredietId=" + kredietId;
                document.location = url + params;               
            });
        </script>
    </body>
</html>
