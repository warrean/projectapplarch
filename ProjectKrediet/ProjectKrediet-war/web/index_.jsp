<%-- 
    Document   : index
    Created on : Nov 16, 2014, 9:06:35 PM
    Author     : groep:Andy Warrens & Pei Tian
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="includes/header.jsp">
    <jsp:param name="title" value="Home"></jsp:param>    
</jsp:include>

	<div class="container">
                <h1 align="center" style="margin-bottom: 24px">Welkom!</h1>
                
                <c:if test='${pageContext.request.isUserInRole("Algemene Werknemer")}'>
                    <p align="center">
                        <a href="werknemer?" role="button" class="btn btn-success">Onkosten Beheren</a>
                    </p>
                </c:if>

                <c:if test='${pageContext.request.isUserInRole("Boekhouder")}'>
                    <p align="center">
                        <a href="boekhouder" role="button" class="btn btn-danger">Kredieten Bekijken (Boekhouder)</a>
                    </p>
                </c:if>

                <c:if test='${pageContext.request.isUserInRole("Manager")}'>
                    <p align="center">
                        <a href="manager?" role="button" class="btn btn-info">Onkosten Keuren (Manager)</a>
                    </p>
                </c:if>
    	</div>   
</body>
</html>
