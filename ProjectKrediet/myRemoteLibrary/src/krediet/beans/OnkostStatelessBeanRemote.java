/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krediet.beans;


import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Remote
public interface OnkostStatelessBeanRemote {
    
    Object maakNieuweOnkostAan(Object oeigenaarObj);
    void onkostOpslaan(String onid, String odatum, String obedrag, String obeschrijving);
    void onkostVerwijderen(String onid);
    List getOnkostenByEigenaarId(Object employee);
    Object getOnkostById(int onkostId);
    
    List getOnkostenVoorGoedkeuring(Object manager);
    void keurOnkostenGoed(List onkostSelectie);
    void keurOnkostenAf(List onkosten);

    void stuurOnkostDoor(Object onkost, Object krediet);


}
