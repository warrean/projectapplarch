/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package krediet.beans;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Remote;
import java.util.List;

/**
 *
 * @author groep:Andy Warrens & Pei Tian
 */
@Remote
public interface KredietStatelessBeanRemote {
    List getKredieten();
    Object getKredietById(int parseInt);

    List getKredietenVoorEmployee(Object employee, BigDecimal bedrag);
    
    Object getEmployee(int employeeid);
    
    int getKeurenAantal(int kredietId,String s);
    BigDecimal getKeurenTotaalBedrag(int kredietId,String s);

}
