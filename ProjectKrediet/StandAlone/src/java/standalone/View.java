/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package standalone;
import java.awt.*; 
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Pei
 */


public class View extends JFrame implements ActionListener{
    private JPanel paneel = new JPanel();
    private JLabel knrl = new JLabel("Geef hieronder KredietNummer in:");
    private JTextField knrt = new JTextField();
    private JLabel info = new JLabel("U vindt KredietDetails bij Standaard Uitvoer van NetBeans!");
    private JButton toon = new JButton("Toon Detail");
    private Main main;
    
    public View(Main main){
        this.main=main;
        toon.addActionListener(this);
        
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        paneel.setLayout(new GridLayout(4,1));
        paneel.add(knrl);
        paneel.add(knrt);
        paneel.add(toon);
        paneel.add(info);
        c.add(paneel);
        setSize(600,200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(toon)){
            int id=Integer.parseInt(knrt.getText());
            main.kredietBeheer(id);
        }
    }
}

