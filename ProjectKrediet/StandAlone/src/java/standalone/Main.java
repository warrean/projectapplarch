/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package standalone;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import krediet.entitybeans.*;
import krediet.beans.KredietStatelessBeanRemote;

/**
 * @author groep Andy & Pei
 */

/**in StandAlone applicatie:
 * Main.java--Controller
 * View.java--View
 * alle Models zitten al in de -ejb project
 **/

public class Main {
    
    @EJB
    private static KredietStatelessBeanRemote krebean=null;
    private View vi;
    
    public Main(){
        this.vi=new View(this);
    }
    
    public void kredietBeheer(int id) {
        
        Krediet krediet=(Krediet)krebean.getKredietById(id);
        krediet.kredietDetailAfdrukken();
        
       List<Onkost> list = (List<Onkost>) krediet.getOnkostCollection();
       int aantal=list.size();
       System.out.println("Totaal Onkosten voor dit krediet: "+aantal);
       
       Iterator it=list.iterator();
       Onkost onkost;
       int i=0;
       while(it.hasNext()){
           i++;
           onkost=(Onkost) it.next();
           System.out.println("");
           System.out.println("Onkost: "+i+"/"+aantal);
           onkost.onkostDetailAfdrukken();
       }
       
       int goed=krebean.getKeurenAantal(id, "goedgekeurd");
       int af=krebean.getKeurenAantal(id, "afgekeurd");
       int door=krebean.getKeurenAantal(id, "doorgestuurd");
       BigDecimal doorg=krebean.getKeurenTotaalBedrag(id, "doorgestuurd");
       BigDecimal goedg=krebean.getKeurenTotaalBedrag(id, "goedgekeurd");
       System.out.println("*******************************************************************************************");
       System.out.println("Analyse:");
       System.out.print("#Aantal Goedgekeurde onkost(en): "+goed+"\t");
       System.out.print("#Aantal Afgekeurde onkost(en): "+af+"\t");
       System.out.print("#Nog te keuren onkost(en): "+door+"\n");
       System.out.print("#Totaal kredietbudget: "+doorg.add(goedg).add(krediet.getKsaldo())+"\t");
       System.out.print("#Betaald bedrag: "+goedg+"\t");
       System.out.print("#Nog te keuren bedrag: "+doorg+"\t");
       System.out.println("#Krediet saldo: "+krediet.getKsaldo());
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("StandAlone applicatie start:");
        Main ma=new Main();
        System.out.println("Ready!");
    }
}
