DROP TABLE IF EXISTS onkost;
DROP TABLE IF EXISTS statusonkost;
DROP TABLE IF EXISTS krediet;
DROP TABLE IF EXISTS employee_functie;
DROP TABLE IF EXISTS functie;
DROP TABLE IF EXISTS employee;

CREATE TABLE employee(
	eid INTEGER PRIMARY KEY,
	enaam VARCHAR(20),
	epaswoord VARCHAR(20),
	ebaasid INTEGER,
	FOREIGN KEY (ebaasid) REFERENCES employee(eid)
);

CREATE TABLE functie (
	fid INTEGER PRIMARY KEY,
	fnaam VARCHAR(20)
);

CREATE TABLE employee_functie (
	efeid INTEGER,
	effid INTEGER,
	primary key (efeid,effid),
	FOREIGN KEY (efeid) REFERENCES employee(eid),
	FOREIGN KEY (effid) REFERENCES functie(fid)
);

CREATE TABLE krediet (
	kid INTEGER PRIMARY KEY,
	kbeschrijving VARCHAR(50),
	kisgewaarborgd BOOLEAN,
	ksaldo DECIMAL(10,2),
	kbeheerderid INTEGER,
	FOREIGN KEY (kbeheerderid) REFERENCES employee(eid)
);

CREATE TABLE statusonkost (
	sid INTEGER PRIMARY KEY,
	snaam VARCHAR(50)
);

CREATE TABLE onkost (
	onid INTEGER PRIMARY KEY,
	odatum DATE,
	obedrag DECIMAL(10,2),
	obeschrijving VARCHAR(50),
	oeigenaarid INTEGER,
	ostatusid INTEGER,
	okredietid INTEGER,
	FOREIGN KEY (oeigenaarid) REFERENCES employee(eid),
	FOREIGN KEY (ostatusid) REFERENCES statusonkost(sid),
	FOREIGN KEY (okredietid) REFERENCES krediet(kid)
);


INSERT INTO employee VALUES (1,'CEO','test1',1);
INSERT INTO employee VALUES (2,'andy','test2',1);
INSERT INTO employee VALUES (3,'pei','test3',1);
INSERT INTO employee VALUES (4,'olivier','test4',2);
INSERT INTO employee VALUES (5,'sven','test5',2);
INSERT INTO employee VALUES (6,'cedric','test6',3);
INSERT INTO employee VALUES (7,'kristof','test7',3);
INSERT INTO employee VALUES (8,'wendy','test8',7);
INSERT INTO employee VALUES (9,'geert','test9',7);

INSERT INTO functie VALUES (1, 'Manager');
INSERT INTO functie VALUES (2, 'Boekhouder');
INSERT INTO functie VALUES (3, 'Algemene Werknemer');

INSERT INTO employee_functie VALUES (1, 1); -- CEO (manager,boekhouder en werknemer)
INSERT INTO employee_functie VALUES (1, 2);
INSERT INTO employee_functie VALUES (1, 3);
INSERT INTO employee_functie VALUES (2, 1); -- andy (manager en werknemer)
INSERT INTO employee_functie VALUES (2, 3);
INSERT INTO employee_functie VALUES (3, 1); -- pei (manager,boekhouder en werknemer)
INSERT INTO employee_functie VALUES (3, 2); 
INSERT INTO employee_functie VALUES (3, 3);
INSERT INTO employee_functie VALUES (4, 2); -- olivier (boekhouder en werknemer)
INSERT INTO employee_functie VALUES (4, 3);
INSERT INTO employee_functie VALUES (5, 3);
INSERT INTO employee_functie VALUES (6, 3);
INSERT INTO employee_functie VALUES (7, 3);-- kristof (manager en werknemer)
INSERT INTO employee_functie VALUES (7, 1);
INSERT INTO employee_functie VALUES (8, 3);
INSERT INTO employee_functie VALUES (9, 3);
INSERT INTO employee_functie VALUES (9, 2);-- geert (boekhouder en werknemer)

-- kredieten (.., saldo's, beheerder)
--ceo kredieten
INSERT INTO krediet VALUES (1, 'Maandelijks budget', TRUE  , 1000, 1);
INSERT INTO krediet VALUES (2, 'Bedrijf budget', FALSE  , 5000, 1);

--andy kredieten
INSERT INTO krediet VALUES (3, 'Computer budget'   , FALSE ,  750, 2);
INSERT INTO krediet VALUES (4, 'Auto budget'       , TRUE  ,  460.01, 2);

--pei kredieten
INSERT INTO krediet VALUES (5, 'Voedsel budget'    , TRUE , 1250, 3);
INSERT INTO krediet VALUES (6, 'Kleren budget'    , FALSE , 2000, 3);
INSERT INTO krediet VALUES (7, 'Activiteit budget'    , FALSE , 380, 3);

--kristof kredieten
INSERT INTO krediet VALUES (8, 'Cafe budget'    , FALSE , 550, 7);







INSERT INTO statusonkost VALUES (1, 'in aanmaak');
INSERT INTO statusonkost VALUES (2, 'doorgestuurd');
INSERT INTO statusonkost VALUES (3, 'goedgekeurd');
INSERT INTO statusonkost VALUES (4, 'afgekeurd');

 -- andy's onkosten (..., eigenaar, status, krediet)
INSERT INTO onkost VALUES (1, '2014-11-1', 134.33, 'Benzine getankt1', 					2, 3, 4);
INSERT INTO onkost VALUES (2, '2014-11-7', 115.67, 'Benzine getankt2', 					2, 2, 4);
INSERT INTO onkost VALUES (3, '2014-11-7',   9.99, 'Ruitenwisser vloeistof gekocht',    2, 2, 4);
INSERT INTO onkost VALUES (4, '2014-11-20', 20.99, 'Olie vervangen',					2, 4, 4);
INSERT INTO onkost VALUES (5, '2014-11-20', 80.00, 'Banden vervangen',					2, 3, 4);

-- olivier's onkosten (extra info: andy is kredietbeheerder van 'computerbudget (kid: 3)')
INSERT INTO onkost VALUES (10, '2014-10-10',   8.00, 'Muis vervangen',					4, 2, 3);
INSERT INTO onkost VALUES (11, '2014-10-11', 180.00, 'Nieuw beeldscherm',				4, 2, 3);
INSERT INTO onkost VALUES (12, '2014-11-20', 100.00, 'Extra geheugen aangekocht',                       4, 2, 3);
INSERT INTO onkost VALUES (13, '2014-12-1',   9.99, 'LED Kerstverlichting',                     	4, 1, 3);
INSERT INTO onkost VALUES (14, '2014-12-2',   16.00, 'Nieuwe stopcontactdozen',				4, 1, 3);

-- pei's onkosten
INSERT INTO onkost VALUES (20, '2014-11-3',  50   , 'Ruitenwisser vloeistof gekocht',	3, 3, 4);
INSERT INTO onkost VALUES (21, '2014-11-1', 150.20, '10kg gemarineerde biefstuk',		3, 4, 5);

-- kristof's onkosten
INSERT INTO onkost VALUES (23, '2014-11-1', 25.50 , '20kg rijst aangevuld', 			7, 2, 5);



